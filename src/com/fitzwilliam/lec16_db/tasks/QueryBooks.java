package com.fitzwilliam.lec16_db.tasks;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author alisovenko
 *         12/7/16.
 */
public class QueryBooks {
    static final String DATABASE_URL = "jdbc:mysql://localhost/books";
    private Connection conn;
    private PreparedStatement sql;

    public QueryBooks() throws SQLException {
        conn = DriverManager.getConnection(DATABASE_URL, "jhtp7", "jhtp7");
        sql = conn.prepareStatement("select isbn " +
                "from titles " +
                "where editionNumber = ? and copyright = ? " +
                "order by isbn asc" );
    }

    /**
     * Queries database table books to fetch all books which editionNumber is equal to {@code editionNumber} and copyright is equal to
     * {@code copyright}.
     *
     * It returns the list of books ISBNs
     */
    public List<String> queryBooksByEditionNumberAndCopyright(int editionNumber, int copyRight) throws SQLException {
        sql.setInt(1, editionNumber);
        sql.setInt(2, copyRight);

        try (ResultSet resultSet = sql.executeQuery()) {

            List<String> result = new ArrayList<>();

            while (resultSet.next()) {
                result.add(resultSet.getString(1));
            }

            return result;
        }
        // 1. set edition number and copyright for prepared statement
        // using "setInt" methods
        // 2. fetch the result set from the prepared statement "sql"
        // 3. iterate through it and add all the rows to the list
//        return null;
    }

    public void closeResources() throws SQLException {
        sql.close();
        conn.close();

    }
}
