package com.fitzwilliam.lec6_arrays.tasks;

/**
 * @author alisovenko
 *         11/2/16.
 */
public class ArraysEquals {
    public static boolean equals(int[] array1, int[] array2) {
        // 1. Check for null parameters
        // if (object == null) or if (object != null)
        // what if both of arguments are null? Only one of them?
        if (array1 == null && array2 == null) {
            return true;
        }
        if (array1 == null || array2 == null) {
            return false;
        }

        // 2. Check for sizes of arrays. If their sizes are not equal - what does it mean?
        if (array1.length != array2.length) {
            return false;
        }

        // 3. Compare elements on the same positions for each element. Use "fori" loop.
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        // null array is equal to another null
        checkEquals(null, null);

        // null array is not equal to non-null
        checkNotEquals(null, new int[0]);
        checkNotEquals(new int[0], null);

        // arrays of different sizes are not equal
        checkNotEquals(new int[0], new int[1]);

        // arrays of the same size but different elements are not equal
        checkNotEquals(new int[]{1, 2, 5}, new int[]{1, 2, 3});

        // equal arrays
        checkEquals(new int[]{6, 5, 4}, new int[]{6, 5, 4});
    }

    private static void checkEquals(int[] a1, int[] a2) {
        boolean result = equals(a1, a2);
        if (result)
            System.out.println("Correct");
        else
            System.out.println("Wrong!");
    }

    private static void checkNotEquals(int[] a1, int[] a2) {
        boolean result = equals(a1, a2);
        if (result)
            System.out.println("Wrong!");
        else
            System.out.println("Correct");
    }
}
