package com.fitzwilliam.lec6_arrays.tasks;

import java.util.Scanner;

public class PrintChart {
    private static int[] readInput() {
        // Read input number, create the array of this size
        // then read new numbers for array in a loop and initialize matching elements in the array
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number of bars: ");
        int numBars = scanner.nextInt();

        if (numBars <= 0) {
            System.out.println("Number of bars must be positive int!");
            return null;
        }
        int[] ans = new int[numBars];

        for (int i = 0; i < numBars; i++) {
            System.out.print("Enter bar " + (i + 1) + ": ");

            int barLength = scanner.nextInt();

            if (barLength < 0) {
                System.out.println("Bar cannot have negative length! Assigning 0.");
                barLength = 0;
            }
            ans[i] = barLength;
        }
        return ans;
    }

    private static void printChart(int[] arr) {
        // Use two loops: the outer one - through the array parameter, the inner one - from 0 to arr[i] (value of the array) and print '*' there
        if (arr == null) {
            System.out.println("WTF? Why array reference is null???");
            return;
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i]; j++) {
                System.out.print('*');
            }
            System.out.printf(" (%s)\n", arr[i]);
        }
    }

    public static void main(String[] args) {
        int[] userArray = readInput();

        if (userArray != null)
            printChart(userArray);
    }
}
