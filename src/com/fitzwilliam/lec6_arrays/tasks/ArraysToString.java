package com.fitzwilliam.lec6_arrays.tasks;

/**
 * @author alisovenko
 *         11/2/16.
 */
public class ArraysToString {
    public static String arrayToString(int[] array) {
        // 1. What happens if argument is null? Check this and return matching result
        if (array == null)
            return "";

        // 2. To create the result create the empty String variable: String result = "".
        // Then iterate through the array and concatenate
        // each element to the result. Also concatenate commas (", ") after this.
        // Check the edge case: if the element is the last - you should not
        // concatenate comma ("1, 2")

        String ans = "";
        for (int i = 0; i < array.length; i++) {
            ans += array[i];
            if (i != array.length - 1)
                ans += ", ";
        }

        return ans;
    }

    public static void main(String[] args) {
        checkArray(null, "");
        checkArray(new int[0], "");
        checkArray(new int[1], "0");
        checkArray(new int[5], "0, 0, 0, 0, 0");
        checkArray(new int[]{5, 30, 99, 2}, "5, 30, 99, 2");
    }

    private static void checkArray(int[] o, String expectedResult) {
        String result = arrayToString(o);

        if (result.equals(expectedResult))
            System.out.println("Correct");
        else
            System.out.printf("Wrong! %s != %s\n", result, expectedResult);
    }
}
