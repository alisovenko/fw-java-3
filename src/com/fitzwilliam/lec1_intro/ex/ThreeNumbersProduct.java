package com.fitzwilliam.lec1_intro.ex;

import java.util.Scanner;


public class ThreeNumbersProduct {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num1, num2, num3;

        System.out.println("Enter three number:");

        num1 = scanner.nextInt();
        num2 = scanner.nextInt();
        num3 = scanner.nextInt();

        long sum = num1 * num2 * num3;

        System.out.println(sum);
    }
}
