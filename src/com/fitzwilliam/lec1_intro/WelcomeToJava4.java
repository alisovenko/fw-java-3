package com.fitzwilliam.lec1_intro;

/*
By Fitzwilliam Institute
 */
public class WelcomeToJava4 {

    public static void main(String[] args) {
        System.out.printf("%s\n%s\n", "Welcome to", "Java Programming!");
    } // end method main
}// end class WelcomeToJava4
    