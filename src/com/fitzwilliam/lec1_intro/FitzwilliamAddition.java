package com.fitzwilliam.lec1_intro;/*
By Fitzwilliam Institute
 */

import java.util.Scanner;


public class FitzwilliamAddition {

    public static void main(String[] args) {// main method
        // create Scanner for user input
        Scanner input = new Scanner(System.in);
        int num1; // first number

        int num2; // second number

        int sum; // sum of number1 and number2

        System.out.print("Enter first number: "); // prompt user
        num1 = input.nextInt(); // read first number from user

        System.out.print("Enter second integer: "); // prompt user
        num2 = input.nextInt(); // read second number from user

        sum = num1 + num2; // addition of the numbers
        System.out.printf("Sum is %d\n", sum); // display result
    } // end method main

} // end class FitzwilliamAddition
