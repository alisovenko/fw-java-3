package com.fitzwilliam.lec1_intro;

/*
By Fitzwilliam Institute
 */
public class WelcomeToJava2 {

    public static void main(String[] args) {
        System.out.print("Welcome to ");
        System.out.println("Java Programming!");
    } // end method main
}// end class WelcomeToJava2
    
