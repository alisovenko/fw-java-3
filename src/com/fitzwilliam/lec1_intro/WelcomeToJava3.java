package com.fitzwilliam.lec1_intro;

/*
By Fitzwilliam Institute
 */
public class WelcomeToJava3 {

    public static void main(String[] args) {
        System.out.println("Welcome\nto\nJava\nProgramming!");
    } // end method main
}// end class WelcomeToJava3
    