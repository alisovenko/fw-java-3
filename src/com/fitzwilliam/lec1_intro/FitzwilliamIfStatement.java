package com.fitzwilliam.lec1_intro;/*
By Fitzwilliam Institute
 */

import java.util.Scanner;


public class FitzwilliamIfStatement {

    public static void main(String[] args) {

        // create Scanner for input
        Scanner input = new Scanner(System.in);
        int num1; // first number declaration
        int num2; // second number declaration

        System.out.print("Enter first number: "); // prompt
        num1 = input.nextInt(); // read first number from user
        System.out.print("Enter second number: "); // prompt
        num2 = input.nextInt(); // read second number from user 

        if (num1 == num2)//are equal
        {
            System.out.printf("%d ==  to %d\n", num1, num2);
        }

        if (num1 != num2)//not equal
        {
            System.out.printf("%d != %d\n", num1, num2);
        }

        if (num1 < num2)//num1 smaller than num2
        {
            System.out.printf("%d < %d\n", num1, num2);
        }
        if (num1 > num2)//num1 greater than num2
        {
            System.out.printf("%d > %d\n", num1, num2);
        }
        if (num1 <= num2)//num1 smaller or equal to num2
        {
            System.out.printf("%d <= %d\n", num1, num2);
        }
        if (num1 >= num2)//num1 greater or equal to num 2
        {
            System.out.printf("%d >= %d\n", num1, num2);
        }
    }

}
