package com.fitzwilliam.lec7_oop_ext.fig08_10_11;// Fig. 8.10: Book.java
// Declare an enum type with constructor and explicit instance fields 
// and accessors for these fields

public enum Book {
    // declare constants of enum type
    Book1("Professional JavaScript for Web Developers", "2009"),
    Book2("Expert Access 2007 Programming", "2007"),
    Book3("Beginning Python", "2005"),
    Book4("Professional C++", "2005"),
    Book5("Professional C#", "2002"),
    Book6("Beginning JavaServer Pages", "2005");

    // instance fields
    private final String title; // book title
    private final String copyrightYear; // copyright year

    // enum constructor
    Book(String bookTitle, String year) {
        title = bookTitle;
        copyrightYear = year;
    } // end enum Book constructor

    // accessor for field title
    public String getTitle() {
        return title;
    } // end method getTitle

    // accessor for field copyrightYear
    public String getCopyrightYear() {
        return copyrightYear;
    } // end method getCopyrightYear
} // end enum Book

