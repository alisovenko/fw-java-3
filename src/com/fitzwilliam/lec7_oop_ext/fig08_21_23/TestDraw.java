package com.fitzwilliam.lec7_oop_ext.fig08_21_23;// Fig. 8.23: TestDraw.java
// Test application to display a DrawPanel.

import javax.swing.*;

public class TestDraw {
    public static void main(String args[]) {
        DrawPanel panel = new DrawPanel();
        JFrame application = new JFrame();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.add(panel);
        application.setSize(300, 300);
        application.setVisible(true);
    } // end main
} // end class TestDraw
