package com.fitzwilliam.lec5_methods;

// Import class random

import java.util.Random;

//By Fitzwilliam
public class RandomNumber2 {

    public static void main(String[] args) {
        //Sample of Random number generator
        // Creating a Random class that will be used to draw the numbers
        Random r = new Random();

        // The draw numbers in the range [0.10] to a variable
        int a = r.nextInt(11); //declaration and variable definition
        System.out.println(a);

        // The draw numbers in the range [-5.15] and display it on the console.
        System.out.println(r.nextInt(21) - 5);
        // 21, because in the interval [-5.15] is 21 numbers and -5,
        // because it is the smallest number in the range.

        // The draw numbers in the range [-20, -10] to a variable.
        a = r.nextInt(11) - 20;
        System.out.println(a);
        // 11, because in the interval [-20.10] is number 11 and -20,
        // because it is the smallest number in the range.

        // The draw of numbers [x, y], where x and y are variables
        // integer of any value.
        int x = 7; // sample value
        int y = 15; // sample value

        a = r.nextInt(y - x + 1) + x; // From (y) subtract smaller (x) and
        // adding 1.
        // then adding smaller (x) cause its
        // a smallest number in the range
        System.out.println(a);

        // Checking the above example for negative numbers:
        x = -27; // sample value
        y = -15; // sample value
        a = r.nextInt(y - x + 1) + x;
        System.out.println(a);

        // ck the above when one number is negative and other is positive.
        // jest ujemna, a druga dodatnia
        x = -7; // sample value
        y = 15; // sample value
        a = r.nextInt(y - x + 1) + x;
        System.out.println(a);

    }

}