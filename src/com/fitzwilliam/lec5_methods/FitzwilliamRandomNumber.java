package com.fitzwilliam.lec5_methods;
//By Fitzwilliam

import java.util.Random;

public class FitzwilliamRandomNumber {

    public static void main(String[] args) {

        Random randomNumbers = new Random(); // random number generator
        int number; // stores each random number that was generated

        // loop 10 times
        for (int counter = 1; counter <= 10; counter++) {
            // pick random integer from 1 to 10
            number = 1 + randomNumbers.nextInt(10);

            System.out.printf("%d  ", number); // display generated value

            // if number is divisible by 5, start a new line
            if (counter % 5 == 0)
                System.out.println();
        } // end for
    } // end main
} // end class 