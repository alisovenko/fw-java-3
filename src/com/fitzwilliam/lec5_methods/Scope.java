package com.fitzwilliam.lec5_methods;

//By Fitzwilliam
public class Scope {
    // field is accessible to all methods of this class
    private int y = 1;

    // method begin create and initialize local variable y
    // and calls methods useLocalVariable and useField
    public void begin() {
        int y = 5; //  local variable y shadows y
        System.out.printf("local variable 'y' in method begin() is %d\n", y);

        useLocalVariable(); // useLocalVariable has local y
        useField(); // useField uses class Scope field y
        useLocalVariable(); // useLocalVariable reinitializes local y
        useField(); // class Scope field y retains its value

        System.out.printf("\nlocal variable 'y' in method begin() is %d\n", y);
    } // end method begin

    // create and initialize local variable y during each call
    public void useLocalVariable() {
        int y = 25; // initialized each time useLocalVariable is called

        System.out.printf(
                "\nlocal variable 'y' on entering method useLocalVariable() is %d\n", y);
        ++y; // modifies this method local variable y
        System.out.printf(
                "local variable 'y' before exiting method useLocalVariable() is %d\n", y);
    } // end method useLocalVariable

    // modify class Scope field y during each call
    public void useField() {
        System.out.printf(
                "\nfield 'y' on entering method useField() is %d\n", y);
        y *= 10; // modifies class Scope field y
        System.out.printf(
                "field 'y' before exiting method useField() is %d\n", y);
    } // end method useField
} // end class 
    
   
