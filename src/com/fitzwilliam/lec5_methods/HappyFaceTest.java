package com.fitzwilliam.lec5_methods;
//By Fitzwilliam
//Test application that display the graphic.

import javax.swing.*;

public class HappyFaceTest {

    public static void main(String[] args) {
        HappyFace panel = new HappyFace();
        JFrame application = new JFrame();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.add(panel);
        application.setSize(230, 250);
        application.setVisible(true);
    }

}
