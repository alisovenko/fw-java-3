package com.fitzwilliam.lec5_methods;

//By Fitzwilliam
public class ScopeTest {

    public static void main(String[] args) {

        // application starting point
        {
            Scope Scope1 = new Scope();
            Scope1.begin();
        }
    }
}
