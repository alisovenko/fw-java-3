package com.fitzwilliam.lec5_methods;
/*
 * By Fitzwilliam
 */

import java.util.Scanner;

public class FitzwilliamFindMax {
    public void determineMax() {
        // create Scanner for input
        Scanner input = new Scanner(System.in);

        // prompt for an input of three values
        System.out.print(
                "Enter three floating-point values separated by spaces: ");
        double number1 = input.nextDouble(); // read first double
        double number2 = input.nextDouble(); // read second double
        double number3 = input.nextDouble(); // read third double

        // method maximum
        double result = maximum(number1, number2, number3);

        // display maximum value
        System.out.println("Maximum value is: " + result);
    } // end method determineMax

    // returns the maximum of its three double parameters
    public double maximum(double x, double y, double z) {
        return Math.max(Math.max(x, y), z);
    } // end method
} // end class