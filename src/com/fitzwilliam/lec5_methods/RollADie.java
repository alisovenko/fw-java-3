package com.fitzwilliam.lec5_methods;
//By Fitzwilliam  

import java.util.Random;

public class RollADie {
    public static void main(String[] args) {
        // Roll a die 20 times. and count frequency of the output
        {
            Random randomNumbers = new Random(); // random number generator

            int frequency1 = 0; // count of number 1 rolled
            int frequency2 = 0; // count of number 2 rolled
            int frequency3 = 0; // count of number 3 rolled
            int frequency4 = 0; // count of number 4 rolled
            int frequency5 = 0; // count of number 5 rolled
            int frequency6 = 0; // count of number 6 rolled

            int number; // stores most recently rolled value

            // summarize results of 20 rolls of a die
            for (int roll = 1; roll <= 20; roll++) {
                number = 1 + randomNumbers.nextInt(6); // number from 1 to 6

                // determine roll value 1-6 and increment appropriate counter
                switch (number) {
                    case 1:
                        ++frequency1; // increment the 1s counter
                        break;
                    case 2:
                        ++frequency2; // increment the 2s counter
                        break;
                    case 3:
                        ++frequency3; // increment the 3s counter
                        break;
                    case 4:
                        ++frequency4; // increment the 4s counter
                        break;
                    case 5:
                        ++frequency5; // increment the 5s counter
                        break;
                    case 6:
                        ++frequency6; // increment the 6s counter
                        break; // optional at end of switch
                } // end switch
            } // end for

            System.out.println("Number\tHow manny times"); // output headers
            System.out.printf("1\t%d\n2\t%d\n3\t%d\n4\t%d\n5\t%d\n6\t%d\n",
                    frequency1, frequency2, frequency3, frequency4,
                    frequency5, frequency6);
        }
    } // end main
} // end class 
   