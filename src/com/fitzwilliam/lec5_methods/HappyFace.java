//By Fitzwilliam
package com.fitzwilliam.lec5_methods;

import javax.swing.*;
import java.awt.*;

public class HappyFace extends JPanel {

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // drawing the face
        g.setColor(Color.GREEN);
        g.fillOval(10, 10, 200, 200);

        // drawing the eyes
        g.setColor(Color.BLACK);
        g.fillOval(55, 65, 30, 30);
        g.fillOval(135, 65, 30, 30);

        // draw the mouth
        g.fillOval(50, 110, 120, 60);

        // creating the shape of a smile
        g.setColor(Color.GREEN);
        g.fillRect(50, 110, 120, 30);
        g.fillOval(50, 120, 120, 40);
    }
}

