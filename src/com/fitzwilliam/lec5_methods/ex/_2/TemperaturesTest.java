package com.fitzwilliam.lec5_methods.ex._2;

public class TemperaturesTest {
    public static void main(String[] args) {
        System.out.println(Temperatures.convertFromCelsiusToFahrenheit(35.5F));
        System.out.println(Temperatures.convertFromCelsiusToKelvin(-23.0F));
        System.out.println(Temperatures.convertFromFahrenheitToCelsius(132.5F));
        System.out.println(Temperatures.convertFromFahrenheitToKelvin(189.5F));
    }
}
