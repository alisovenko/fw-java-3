package com.fitzwilliam.lec5_methods.ex._2;

public class Temperatures {
    public static float convertFromCelsiusToKelvin(float celsius) {
        return celsius + 273.15f;

    }

    public static float convertFromCelsiusToFahrenheit(float celsius) {
        return celsius * 1.8f + 32;
    }

    public static float convertFromFahrenheitToCelsius(float fahrenheit) {
        return (fahrenheit - 32) / 1.8f;
    }
    public static float convertFromFahrenheitToKelvin(float fahrenheit) {
        return (fahrenheit + 459.67f) * 5f/9f;
    }
}
