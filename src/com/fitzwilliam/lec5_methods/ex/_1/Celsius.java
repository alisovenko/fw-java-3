package com.fitzwilliam.lec5_methods.ex._1;

public class Celsius {
    private final float temperature;

    public Celsius(float temperature) {
        this.temperature = temperature;
    }

    public float convertToFahrenheit() {
        // no other fields
        // don't change the temperature itself
        return temperature * 1.8f + 32;
    }

    public float convertToKelvin() {
        return temperature + 273.15f;
    }
}
