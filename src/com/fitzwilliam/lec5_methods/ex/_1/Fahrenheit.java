package com.fitzwilliam.lec5_methods.ex._1;

public class Fahrenheit {
    private final float temperature;

    public Fahrenheit(float temperature) {
        this.temperature = temperature;
    }

    public float convertToCelsius() {
        return (temperature - 32) / 1.8f;
    }

    public float convertToKelvin() {
        return (temperature + 459.67f) * 5f/9f;
    }
}
