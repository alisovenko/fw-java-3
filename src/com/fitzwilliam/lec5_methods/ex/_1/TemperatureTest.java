package com.fitzwilliam.lec5_methods.ex._1;

public class TemperatureTest {
    public static void main(String[] args) {
        Celsius celsius = new Celsius(40);
        System.out.println(celsius.convertToFahrenheit());
        System.out.println(celsius.convertToFahrenheit());
        System.out.println(celsius.convertToKelvin());

        Fahrenheit fahrenheit = new Fahrenheit(400);

        System.out.println(fahrenheit.convertToKelvin());
        System.out.println(fahrenheit.convertToCelsius());
    }
}
