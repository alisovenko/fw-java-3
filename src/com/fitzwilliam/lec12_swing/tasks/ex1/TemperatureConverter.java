package com.fitzwilliam.lec12_swing.tasks.ex1;

import com.fitzwilliam.lec12_swing.fig11_06_07.LabelFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TemperatureConverter extends JFrame{
    private JLabel output;
    private JTextField textField;

    public TemperatureConverter() throws HeadlessException {
        super("Temp converter");
        setLayout(new FlowLayout());

        output = new JLabel("<Your convertion will be here>");
        textField = new JTextField(10);

        add(output);
        add(textField);

        textField.addActionListener(new TempListener());
    }

    private class TempListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            final String textValue = e.getActionCommand();

            double fehrenheit = Double.parseDouble(textValue);
            double celcius = (double) 5 / 9 * (fehrenheit - 32);

            output.setText(String.valueOf(celcius));
        }
    }

    public static void main(String[] args) {
        TemperatureConverter tempFrame = new TemperatureConverter(); // create LabelFrame
        tempFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tempFrame.setSize(500, 400); // set frame size
        tempFrame.setVisible(true); // display frame
    }
}
