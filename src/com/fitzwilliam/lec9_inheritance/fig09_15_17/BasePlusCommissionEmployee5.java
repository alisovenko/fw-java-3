package com.fitzwilliam.lec9_inheritance.fig09_15_17;// Fig. 9.16: BasePlusCommissionEmployee5.java
// BasePlusCommissionEmployee5 class declaration.

public class BasePlusCommissionEmployee5 extends CommissionEmployee4 {
    private double baseSalary; // base salary per week

    // six-argument constructor
    public BasePlusCommissionEmployee5(String first, String last,
                                       String ssn, double sales, double rate, double salary) {
        super(first, last, ssn, sales, rate);
        setBaseSalary(salary); // validate and store base salary

        System.out.printf(
                "\nBasePlusCommissionEmployee5 constructor:\n%s\n", this);
    } // end six-argument BasePlusCommissionEmployee5 constructor

    // set base salary
    public void setBaseSalary(double salary) {
        baseSalary = (salary < 0.0) ? 0.0 : salary;
    } // end method setBaseSalary

    // return base salary
    public double getBaseSalary() {
        return baseSalary;
    } // end method getBaseSalary

    // calculate earnings
    public double earnings() {
        return getBaseSalary() + super.earnings();
    } // end method earnings

    // return String representation of BasePlusCommissionEmployee5
    public String toString() {
        return String.format("%s %s\n%s: %.2f", "base-salaried",
                super.toString(), "base salary", getBaseSalary());
    } // end method toString
} // end class BasePlusCommissionEmployee5

