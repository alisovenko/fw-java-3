package com.fitzwilliam.lec10_polymorphism.task;

public class Dropbox extends AbstractFlatCloud {
    private String[] settings = new String[0];

    public Dropbox(int capacity) {
        super(capacity);
    }

    @Override
    public void updateSettings(String properties) {
        settings = properties.split(",");
    }

    @Override
    public String readSettings() {
        String ans = "Dropbox settings: ";
        for (String setting : settings) {
            ans += setting + ",";
        }
        return ans.substring(0, ans.length() - 1);
    }

}
