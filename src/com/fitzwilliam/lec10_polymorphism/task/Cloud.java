package com.fitzwilliam.lec10_polymorphism.task;

import java.util.List;

public interface Cloud {
    boolean addFile(String file);

    List<String> getAllFiles();

    boolean sync();

    void updateSettings(String properties);

    String readSettings();
}
