package com.fitzwilliam.lec10_polymorphism.task;

public class GoogleCloud extends AbstractFlatCloud {
    private String settings;

    public GoogleCloud(int capacity) {
        super(capacity);
    }

    @Override
    public void updateSettings(String properties) {
        this.settings = "Google Cloud settings: " + properties;
    }

    @Override
    public String readSettings() {
        return settings;
    }

}
