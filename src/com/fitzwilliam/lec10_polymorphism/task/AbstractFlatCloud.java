package com.fitzwilliam.lec10_polymorphism.task;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFlatCloud implements Cloud {
    private List<String> newFiles;
    private List<String> syncedFiles;
    private int capacity;

    protected AbstractFlatCloud(int capacity) {
        this.capacity = capacity;
        newFiles = new ArrayList<>(capacity);
        syncedFiles = new ArrayList<>(capacity);
    }

    @Override
    public boolean addFile(String file) {
        if (newFiles.size() == capacity) {
            return false;
        }
        newFiles.add(file);
        return true;
    }

    @Override
    public List<String> getAllFiles() {
        return syncedFiles;
    }

    @Override
    public boolean sync() {
        if (syncedFiles.size() + newFiles.size() > capacity) {
            System.out.println("Sync failed!");
            return false;
        }
        syncedFiles.addAll(newFiles);
        newFiles.clear();
        System.out.println("Sync successful!");
        return true;
    }

    @Override
    public String toString() {
        return String.format("New files :%d, synced files: %d [capacity :%d]", newFiles.size(), syncedFiles.size(), capacity);
    }
}
