package com.fitzwilliam.lec10_polymorphism.task;

public class CloudChecker {
    public static void check(Cloud cloud) {
        System.out.println("\n*** Test for cloud provider: " + cloud.getClass().getSimpleName());

        // Adding one file
        System.out.println("Adding file \"test\"");
        cloud.addFile("test");
        System.out.println(cloud.getAllFiles());

        System.out.println("Syncing...");
        cloud.sync();
        System.out.println(cloud.getAllFiles());

        // TODO add the second file ("test 2") to the cloud, call sync() method and output all the files in cloud after this
        // ......

        // TODO update the settings object for cloud and output updated one
        cloud.updateSettings("new settings");
        cloud.readSettings();
    }
}
