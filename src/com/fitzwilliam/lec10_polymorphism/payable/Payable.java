package com.fitzwilliam.lec10_polymorphism.payable;
// Payable interface declaration.

public interface Payable {
    double getPaymentAmount(); // calculate payment; no implementation
}
