package com.fitzwilliam.lec10_polymorphism.animals;

/**
 * @author alisovenko
 *         11/8/16.
 */
public class Cat extends Animal {
    @Override
    public int getAge() {
        return 0;
    }

    @Override
    public boolean isHotBlooded() {
        return true;
    }

    @Override
    public String toString() {
        return "I'm cat";
    }
}
