package com.fitzwilliam.lec10_polymorphism.animals;

/**
 * @author alisovenko
 *         11/8/16.
 */
public class Dog extends Animal {
    @Override
    public boolean isHotBlooded() {
        return true;
    }

    @Override
    public String toString() {
        return "I'm dog";
    }

    public void bark() {
        System.out.println("Woof");
    }
}
