package com.fitzwilliam.lec10_polymorphism.animals;

/**
 * @author alisovenko
 *         11/8/16.
 */
public class Crocodile extends Animal {
    @Override
    public boolean isHotBlooded() {
        return false;
    }
}
