package com.fitzwilliam.hometasks._4;

import java.util.Arrays;

public class SwapArraysReverse {
    public static void main(String[] args) {
        int[] a = {1, 2, 3};
        int[] b = {5, 4, 3};
        swap(a, b);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));

        a = new int[]{2, 3};
        b = new int[]{10, 4, 3};
        swap(a, b);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));


        a = null;
        b = new int[]{10, 4, 3};
        swap(a, b);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
    }
    public static boolean swap(int[] array1, int[] array2) {
        if (array1 == null || array2 == null || array1.length != array2.length || array1.length == 0) {
            return false;
        }

        final int n = array1.length;
        for (int i = 0, j = n - i - 1; i < n; i++, j--) {
            int temp = array1[i];
            array1[i] = array2[j];
            array2[j] = temp;
        }
        return true;
    }
}
