package com.fitzwilliam.hometasks._4;

public class VerticalHistogram {
    public static void main(String[] args) {
        int[] histo = new int[]{3, 0, 5, 2, 7, 9, 2, 1};
        printHistogram(histo);
    }

    private static void printHistogram(int[] marks) {
        if (marks == null || marks.length == 0) {
            return;
        }
        int maxBar = 0;

        for (int mark : marks) {
            maxBar = Math.max(maxBar, mark);
        }

        for (; maxBar > 0; maxBar--) {
            for (int mark : marks) {
                if (mark >= maxBar) {
                    System.out.printf("%2s", '*');
                } else {
                    System.out.printf("%2s", ' ');
                }
            }
            System.out.println();
        }
        for (int i = 0; i < marks.length; i++) {
            System.out.printf("%2s", i + 1);
        }
        System.out.println();
    }
}
