package com.fitzwilliam.hometasks._4;

import java.util.Arrays;

public class ArraysIntersect {
    public static void main(String[] args) {
        int[] a = {1, 4, 5, 8};
        int[] b = {2, 3, 4};
        System.out.println(Arrays.toString(findIntersection1(a, b)));
        System.out.println(Arrays.toString(findIntersection2(a, b)));

        a = null;
        b = new int[]{2, 3, 4};
        System.out.println(Arrays.toString(findIntersection1(a, b)));
        System.out.println(Arrays.toString(findIntersection2(a, b)));


        a = new int[]{2, 3, 4};
        b = new int[]{2, 3, 4};
        System.out.println(Arrays.toString(findIntersection1(a, b)));
        System.out.println(Arrays.toString(findIntersection2(a, b)));


        a = new int[]{1, 2, 3, 5};
        b = new int[]{2, 3, 4, 7, 1};
        System.out.println(Arrays.toString(findIntersection1(a, b)));
        System.out.println(Arrays.toString(findIntersection2(a, b)));

        a = new int[]{1, 0, 2, 3, 5};
        b = new int[]{2, 3, 0, 4, 7, 1};
        System.out.println(Arrays.toString(findIntersection1(a, b)));
        System.out.println(Arrays.toString(findIntersection2(a, b)));
    }

    public static int[] findIntersection1(int[] first, int[] second) {
        if (first == null || second == null || first.length == 0 || second.length == 0) {
            return new int[0];
        }

        int numberOfIntersected = 0;

        // First we find how many elements intersect
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                if (first[i] == second[j]) {
                    numberOfIntersected++;
                    break;
                }
            }
        }
        int[] ans = new int[numberOfIntersected];
        int p = 0;

        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                if (first[i] == second[j]) {
                    ans[p++] = first[i];
                    break;
                }
            }
        }
        return ans;
    }
    public static int[] findIntersection2(int[] first, int[] second) {
        if (first == null || second == null || first.length == 0 || second.length == 0) {
            return new int[0];
        }

        int[] ans = new int[Math.min(first.length, second.length)];
        int p = 0;

        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                if (first[i] == second[j]) {
                    ans[p++] = first[i];
                    break;
                }
            }
        }

        // if we filled the array - there is no need to copy it to new one
        if (p == ans.length) {
            return ans;
        }
        // now we need to copy 0..p elements from ans to new one
        int[] intersected = new int[p];
        System.arraycopy(ans, 0, intersected, 0, p);
        return intersected;
    }
}
