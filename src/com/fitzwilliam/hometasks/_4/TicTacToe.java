package com.fitzwilliam.hometasks._4;

public class TicTacToe {
    public static void main(String[] args) {
        boolean[][] board = {{true, false, true}, {true, true, false}, {true, false, false}};
        checkTheBoard(board);

        board = new boolean[][]{{true, true}, {true, true}};
        checkTheBoard(board);

        board = new boolean[][]{{true, false, true}, {false, true, false}, {true, false, true}};
        checkTheBoard(board);

    }

    // Note that given algorithm is not the most performant as uses three array scans although everything can be done in
    // one loop
    private static void checkTheBoard(boolean[][] board) {
        if (board == null || board.length == 0) {
            return;
        }
        int totalCrossed = 0;

        // check horizontals
        for (int i = 0; i < board.length; i++) {
            int j;
            for (j = 0; j < board[i].length; j++) {
                if (!board[i][j])
                    break;
            }
            if (j == board[i].length) {
                System.out.println("Found the horizontal crossed line: " + i);
            }
        }
        // check verticals
        for (int i = 0; i < board[0].length; i++) {
            int j;
            for (j = 0; j < board.length; j++) {
                if (!board[j][i])
                    break;
            }
            if (j == board.length) {
                System.out.println("Found the vertical crossed line: " + i);
            }
        }
        // check diagonals
        boolean leftDiagonal = true, rightDiagonal = true;
        for (int i = 0; i < board.length; i++) {
            leftDiagonal = leftDiagonal && board[i][i];
            rightDiagonal = rightDiagonal && board[i][board[0].length - 1 - i];
        }
        if (leftDiagonal)
            System.out.println("Found diagonal from left-top to right-bottom.");
        if (rightDiagonal)
            System.out.println("Found diagonal from right-top to left-bottom.");

        System.out.println();
    }
}
