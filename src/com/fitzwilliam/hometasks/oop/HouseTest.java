package com.fitzwilliam.hometasks.oop;

public class HouseTest {
    public static void main(String[] args) {
        Person person = new Person("John", "Snow");
        House house = new House("Baggoth street", 3, person);

        house.display();

        Person person2 = new Person();
        person2.setName("Bill");
        person2.setLastName("Gates");

        House house2 = new House();
        house2.setAddress("NY, Times sq, 5/4");
        house2.setNumberOfRooms(7);
        house2.setOwner(person2);

        house2.display();
    }
}
