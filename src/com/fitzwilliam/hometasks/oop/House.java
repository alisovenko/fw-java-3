package com.fitzwilliam.hometasks.oop;

public class House {
    private String address;
    private Person owner;
    private int numberOfRooms;

    public House(String address, int numberOfRooms, Person owner) {
        this.address = address;
        this.owner = owner;
        this.numberOfRooms = numberOfRooms;
    }

    public House() {

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public void display() {
        System.out.printf("Address = %s, number of rooms = %d, owner name = %s, owner lastName = %s\n", address, numberOfRooms, owner.getName(), owner.getLastName());
    }
}
