package com.fitzwilliam.hometasks._2;

public class CalculatorOOPTest {
    public static void main(String[] args) {
        final CalculatorOOP calculator = new CalculatorOOP(5, 6);

        calculator.displaySum();
        calculator.displayDifference();
        calculator.displayMultiplication();
        calculator.displayDivision();
    }
}
