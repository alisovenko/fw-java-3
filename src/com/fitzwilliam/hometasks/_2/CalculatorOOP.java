package com.fitzwilliam.hometasks._2;

public class CalculatorOOP {
    private int firstNum;

    private int secondNum;

    public CalculatorOOP(int firstNum, int secondNum) {
        this.firstNum = firstNum;
        this.secondNum = secondNum;
    }

    public int getFirstNum() {
        return firstNum;
    }

    public void setFirstNum(int firstNum) {
        this.firstNum = firstNum;
    }

    public int getSecondNum() {
        return secondNum;
    }

    public void setSecondNum(int secondNum) {
        this.secondNum = secondNum;
    }

    public void displaySum() {
        System.out.printf("Sum: %d\n", (long) firstNum + secondNum);
    }

    public void displayDifference() {
        System.out.printf("Difference: %d\n", (long) firstNum - secondNum);
    }

    public void displayMultiplication() {
        System.out.printf("Multiplication: %d\n", (long) firstNum * secondNum);
    }

    public void displayDivision() {
        if (secondNum == 0) {
            System.out.println("Cannot perform the division to 0!");
            return;
        }
        System.out.printf("Division: %d\n", firstNum / secondNum);
    }
}
