package com.fitzwilliam.hometasks._2;

public class CustomerTest {
    public static void main(String[] args) {
        // first customer (created using constructor)
        Address address = new Address(5, "Bacon street", "London");
        Customer customer = new Customer(1, "British Petrolium inc.", address);

        customer.display();

        // second customer (created using setter methods)
        Address address2 = new Address();
        Customer customer2 = new Customer();

        address2.setHouseNumber(10);
        address2.setStreet("Croissant av.");
        address2.setCity("Paris");

        customer2.setId(10);
        customer2.setName("Sweet Bakery");
        customer2.setAddress(address2);

        customer2.display();
    }
}
