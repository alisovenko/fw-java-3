package com.fitzwilliam.hometasks._1;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the first number:");
        int num1 = scanner.nextInt();

        System.out.println("Enter the operator:");
        String line = scanner.next();
        char operator = line.charAt(0);

        System.out.println("Enter the second number:");
        int num2 = scanner.nextInt();

        int ans = 0;
        if (operator == '-')
            ans = num1 - num2;

        if (operator == '+')
            ans = num1 + num2;

        if (operator == '*')
            ans = num1 * num2;

        if (operator == '/')
            ans = num1 / num2;

        System.out.printf("%d %s %d = %d\n", num1, operator, num2, ans);
    }
}
