package com.fitzwilliam.hometasks._1;

import java.util.Scanner;

public class Grid3x3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Write 1 or O for cell [1, 1]:");
        int _11 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [1, 2]:");
        int _12 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [1, 3]:");
        int _13 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [2, 1]:");
        int _21 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [2, 2]:");
        int _22 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [2, 3]:");
        int _23 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [3, 1]:");
        int _31 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [3, 2]:");
        int _32 = scanner.nextInt();
        System.out.println("Write 1 or O for cell [3, 3]:");
        int _33 = scanner.nextInt();

        System.out.println();
        System.out.printf("%d %d %d\n", _11, _12, _13);
        System.out.printf("%d %d %d\n", _21, _22, _23);
        System.out.printf("%d %d %d\n", _31, _32, _33);
    }
}
