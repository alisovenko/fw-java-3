package com.fitzwilliam.hometasks._3;

public class HalfDiamondTest {
    public static void main(String[] args) {
        int n = 55;
        int width = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print('*');
            }
            System.out.println();
            if (i < n / 2)
                width++;
            else
                width--;
        }
    }
}
