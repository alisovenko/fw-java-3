package com.fitzwilliam.hometasks._3;

import java.util.Scanner;

public class TaxCalculator {
    public static void main(String[] args) {
        System.out.println("Enter employees salaries one by one or -1 to show final tax calculation results.\n");

        Scanner scanner = new Scanner(System.in);
        double tax20 = 0.0;
        double tax40 = 0.0;
        double totalSalary = 0.0;
        int salariesCount = 0;

        while (true) {
            System.out.print("Enter salary as a floating point number: ");
            float salary = scanner.nextFloat();

            if (salary == -1)
                break;

            if (salary < 700) {
                tax20 += salary * 0.2;
            } else {
                tax20 += 700 * 0.2;
                tax40 += (salary - 700) * 0.4;
            }
            salariesCount++;
            totalSalary += salary;
        }

        double totalTaxes = tax20 + tax40;
        double avgSalary = salariesCount > 0 ? totalSalary / salariesCount : 0;
        double avgTaxes = salariesCount > 0 ? totalTaxes / salariesCount : 0;

        System.out.println("\n### Tax Calculation Results ###\n");
        System.out.printf("%-20s: %10d\n", "Number of Employees", salariesCount);
        System.out.printf("%-20s: %10.2f\n", "Total Salary", totalSalary);
        System.out.printf("%-20s: %10.2f\n", "Total Taxes", totalTaxes);
        System.out.printf("%-20s: %10.2f\n", "Total Taxes (at 20%)", tax20);
        System.out.printf("%-20s: %10.2f\n", "Total Taxes (at 40%)", tax40);
        System.out.printf("%-20s: %10.2f\n", "Average Salary", avgSalary);
        System.out.printf("%-20s: %10.2f\n", "Average Tax", avgTaxes);

        System.out.println("\n### Histogram ### \n");

        int taxesSteps = (int) (totalTaxes * 50 / totalSalary);
        System.out.printf("%-20s", "Total salary:");
        for (int i = 0; i < 50; i++) {
            System.out.print("*");
        }
        System.out.println();
        System.out.printf("%-20s", "Total tax:");
        for (int i = 0; i < taxesSteps; i++) {
            System.out.print("*");
        }
    }
}
