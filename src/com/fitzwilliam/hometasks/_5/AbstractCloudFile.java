package com.fitzwilliam.hometasks._5;

import java.time.LocalDateTime;

public abstract class AbstractCloudFile implements CloudFile {
    private String name;
    private final LocalDateTime dateTimeCreated;
    private byte[] data;
    private FileType fileType;

    public AbstractCloudFile(String name, LocalDateTime dateTimeCreated, FileType fileType, byte[] data) {
        this.name = name;
        this.dateTimeCreated = dateTimeCreated;
        this.fileType = fileType;
        this.data = data;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public FileType getFileType() {
        return fileType;
    }

    @Override
    public LocalDateTime getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
