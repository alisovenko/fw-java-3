package com.fitzwilliam.hometasks._5;

import java.time.LocalDateTime;

public class CloudChecker {
    public static void check(Cloud cloud) {
        System.out.println("\n*** Test for cloud provider: " + cloud.getClass().getSimpleName());

        // Adding one file
        System.out.println("Adding file \"test\"");
        cloud.addFile(new TextCloudFile("readme.txt", LocalDateTime.parse("2016-11-11T10:20:30"), "This is some text".getBytes()));
        System.out.println(cloud.getAllFiles());

        System.out.println("Syncing...");
        cloud.sync();
        System.out.println(cloud.getAllFiles());

        // Adding second file
        System.out.println("Adding file \"test 2\" and syncing");
        cloud.addFile(new ImageCloudFile("baloon.jpg", LocalDateTime.parse("2017-01-20T23:10:00"), new byte[]{0x11, 0x1A, 0x2B, 0x5F}));
        cloud.sync();
        System.out.println(cloud.getAllFiles());

        cloud.updateSettings("new settings");
        cloud.readSettings();
    }
}
