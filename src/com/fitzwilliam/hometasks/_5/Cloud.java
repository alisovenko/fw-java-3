package com.fitzwilliam.hometasks._5;

import java.util.List;

public interface Cloud {
    boolean addFile(CloudFile file);

    List<CloudFile> getAllFiles();

    boolean sync();

    void updateSettings(String properties);

    String readSettings();
}
