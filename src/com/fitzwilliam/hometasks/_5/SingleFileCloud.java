package com.fitzwilliam.hometasks._5;

import java.util.Collections;
import java.util.List;

public class SingleFileCloud implements Cloud {
    private CloudFile newFile;
    private CloudFile syncFile;
    private String settings;

    @Override
    public boolean addFile(CloudFile file) {
        newFile = file;
        return true;
    }

    @Override
    public List<CloudFile> getAllFiles() {
        return Collections.singletonList(syncFile);
    }

    @Override
    public boolean sync() {
        if (newFile == null) {
            return false;
        }
        syncFile = newFile;
        newFile = null;
        return true;
    }

    @Override
    public void updateSettings(String properties) {
        settings = "Single file cloud settings: " + properties;
    }

    @Override
    public String readSettings() {
        return settings;
    }

}
