package com.fitzwilliam.hometasks._5;

import java.time.LocalDateTime;

public class VideoCloudFile extends AbstractCloudFile {
    public VideoCloudFile(String name, LocalDateTime dateTimeCreated, byte[] data) {
        super(name, dateTimeCreated, FileType.VIDEO, data);
    }

    @Override
    public String getIcon() {
        return "some video icon";
    }

    @Override
    public void preview() {
        System.out.println("Rendering the video file preview");
    }
}
