package com.fitzwilliam.hometasks._5;

public enum FileType {
    TEXT, IMAGE, VIDEO
}
