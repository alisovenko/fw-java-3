package com.fitzwilliam.hometasks._5;

import java.time.LocalDateTime;

public interface CloudFile {
    /**
     * Returns the name of file
     */
    String getName();

    /**
     * Returns file type
     */
    FileType getFileType();

    /**
     * Returns the date and time was created.
     */
    LocalDateTime getDateTimeCreated();

    /**
     * Returns the icon that should be displayed for the file.
     */
    String getIcon();

    /**
     * Renders some "preview" for the file.
     */
    void preview();
}
