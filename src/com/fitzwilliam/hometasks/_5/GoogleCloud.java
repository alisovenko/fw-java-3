package com.fitzwilliam.hometasks._5;

public class GoogleCloud extends AbstractFlatCloud {
    private String settings;
    private String account;

    public GoogleCloud(int capacity, String account) {
        super(capacity);
        this.account = account;
    }

    @Override
    public void updateSettings(String properties) {
        this.settings = "Google Cloud settings: " + properties;
    }

    @Override
    public String readSettings() {
        return settings;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", settings: %s, account: %s", readSettings(), account);
    }
}
