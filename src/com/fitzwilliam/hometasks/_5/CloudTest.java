package com.fitzwilliam.hometasks._5;

public class CloudTest {
    public static void main(String[] args) {
        Cloud[] providers = new Cloud[3];
        providers[0] = new GoogleCloud(4, "a@gmail.com");
        providers[1] = new SingleFileCloud();
        providers[2] = new Dropbox(5);

        for (Cloud provider : providers) {
            CloudChecker.check(provider);
            System.out.println(provider.toString());

            for (CloudFile cloudFile : provider.getAllFiles()) {
                System.out.printf("File: %s (type: %s), created: %s\n", cloudFile.getName(), cloudFile.getFileType(), cloudFile.getDateTimeCreated());
            }
        }
    }
}
