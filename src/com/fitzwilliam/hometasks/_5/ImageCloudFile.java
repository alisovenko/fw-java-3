package com.fitzwilliam.hometasks._5;

import java.time.LocalDateTime;

public class ImageCloudFile extends AbstractCloudFile {
    public ImageCloudFile(String name, LocalDateTime dateTimeCreated, byte[] data) {
        super(name, dateTimeCreated, FileType.IMAGE, data);
    }

    @Override
    public String getIcon() {
        return "some image icon";
    }

    @Override
    public void preview() {
        System.out.println("Rendering the image file preview");
    }
}
