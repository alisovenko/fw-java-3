package com.fitzwilliam.hometasks._5;

import java.time.LocalDateTime;

public class TextCloudFile extends AbstractCloudFile {
    public TextCloudFile(String name, LocalDateTime dateTimeCreated, byte[] data) {
        super(name, dateTimeCreated, FileType.TEXT, data);
    }

    @Override
    public String getIcon() {
        return "Some icon for text files";
    }

    @Override
    public void preview() {
        System.out.println("Streaming the text to the client for preview rendering");
    }
}
