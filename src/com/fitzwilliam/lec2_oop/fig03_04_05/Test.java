package com.fitzwilliam.lec2_oop.fig03_04_05;

public class Test {
    public static void main(String args[]) {
        int x = 30;
        int y = 10;

        if (x == 30) {
            if (y == 10)
                System.out.print("Y = 10");
             else
                System.out.println("Y != 10");

            System.out.println("X == 30");
        }
    }
}
