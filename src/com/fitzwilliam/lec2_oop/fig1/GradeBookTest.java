package com.fitzwilliam.lec2_oop.fig1;

import java.util.Scanner;


/**
 * @author alisovenko
 *         4/3/17.
 */
public class GradeBookTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        GradeBook gradeBook = new GradeBook("Fooo!!!!");

        System.out.println("Enter the message!");

        String msg = scanner.nextLine();

        gradeBook.setCourseName(msg);

        gradeBook.displayMessage();
    }
}
